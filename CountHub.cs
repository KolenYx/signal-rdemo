﻿using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.SignalR;
using SingalRDemos.Services;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;

namespace SingalRDemos
{
    //[Authorize]
    public class CountHub : Hub
    {
        private readonly CountService _countService;

        public CountHub(CountService countService)
        {
            _countService = countService;
        }

        public async Task GetLatestCount(string random)
        {
            int count;
            do
            {
                count = _countService.GetLatestCount();
                Thread.Sleep(1000);
                await Clients.All.SendAsync("ReceiveUpdate", count);
            } while (count < 10);
            await Clients.All.SendAsync("Finished");
        }

        public override async Task OnConnectedAsync()
        {
            //var user = Context.User.Identity.Name;
            //var connectionId = Context.ConnectionId;
            //var client = Clients.Client(connectionId);
            //await client.SendAsync("someFunc", new { });
            //await Clients.AllExcept(connectionId).SendAsync("someFunc", new { });

            //await Groups.AddToGroupAsync(connectionId, "MyGroup");

            //await Groups.RemoveFromGroupAsync(connectionId, "MyGroup");

            //await Clients.Groups("MyGroup").SendAsync("someFunc");

        }
    }
}
